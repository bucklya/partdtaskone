package com.example.partdtaskone;

public interface OnItemClickListener {
    void onItemClick (Product bookTitle);
}
