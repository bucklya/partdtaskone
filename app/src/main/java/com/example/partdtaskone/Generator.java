package com.example.partdtaskone;

import java.util.ArrayList;
import java.util.List;

public final class Generator {
    private Generator() {
    }

    public static List<Product> generate() {
        List<Product> list = new ArrayList<>();
        list.add(new Product("Квантовые вычисления для настоящих айтишников", "Бернхард К.", 1099.99d));
        list.add(new Product("Reactive Programming with Kotlin - Learn Rx with RxJava, RxKotlin and RxAndroid", "Alex Sullivan", 2300d));
        list.add(new Product("Advanced Android App Architecture - Real-world app architecture in Kotlin 1.3", "Yun Cheng, Aldo Olivares Dominguez", 1499.89d));
        list.add(new Product("Карьера программиста", "Гейл Лакман Макдауэлл", 849.9d));
        list.add(new Product("Android Studio 3.3 Development Essentials", "Neil Smyth", 1600d));
        list.add(new Product("Head First. Программирование для Android", "Гриффитс Дэвид, Гриффитс Дон", 1300d));
        list.add(new Product("The Busy Coder's Guide to Android Development", "Mark L. Murphy", 1730.49d));
        list.add(new Product("Professional Android, 4th edition", "eto Meier, Ian Lake", 1999.99d));
        list.add(new Product("Android. Программирование для профессионалов. 3-е издание.", "Филлипс Б., Стюарт К., Марсикано К.", 4500d));
        list.add(new Product("Kotlin for Android Developers", "Antonio Leiva", 2600d));
        list.add(new Product("Swift для детей. Самоучитель по созданию приложений для iOS", "Глория Уинквист, Мэтт Маккарти", 999.99d));
        list.add(new Product("Универсальное устройство: Неизвестная история создания iPhone", "Брайан Мерчант", 1236.49d));
        list.add(new Product("Swift. Основы разработки приложений под iOS, iPadOS и macOS", "Василий Усов", 1400d));
        list.add(new Product("Advanced iOS App Architecture", "Rene Cacheaux, Josh Berlin", 1870d));
        list.add(new Product("Push Notifications by Tutorials - Mastering Push Notifications in iOS (2е издание)", "Scott Grosch", 1389.99d));
        list.add(new Product("Design Patterns by Tutorials - Learning Design Patterns in Swift (3е издание)", "Joshua Greene, Jay Strawn", 1200d));
        list.add(new Product("RxSwift. Reactive Programming with Swift (3 издание)", "Florent Pillet, Marin Todorov, Scott Gardner", 1700.99d));
        list.add(new Product("Learning React Native, 2nd edition", "Bonnie Eisenman", 2349.99d));
        list.add(new Product("iOS 11 & Swift 4 for Beginners", "Fahim Farook, Matt Galloway, Eli Ganim, Matthijs Hollemans, Ben Morrow, Cosmin", 1689.99d));
        list.add(new Product("Beginning iPhone Development with Swift 4: Exploring the iOS SDK", "Молли Маскри, Ким Топли, Дэвид Марк,Фредрик Т. Олссон, Джефф Ламарш", 2280d));
        list.add(new Product("Swift in Depth", "Tjeerd in 't Veen", 1800d));
        list.add(new Product("Fullstack React Native. The complete guide to React Native.", "Devin Abbott, Houssein Djirdeh, Anthony Accomazzo, and Sophia Shoemaker", 2399.99d));
        list.add(new Product("React Native Cookbook", "Jonathan Lebensold", 1800d));
        list.add(new Product("Hands-On Design Patterns with React Native", "Mateusz Grzesiukiewicz", 1799.79d));
        list.add(new Product("React Native for Mobile Development, 2nd edition", "Akshat Paul, Abhishek Nalwaya", 1777.77d));
        list.add(new Product("React Native Cookbook", "Dan Ward", 987.65d));
        list.add(new Product("Swift. Основы разработки приложений под iOS и macOS. 3 издание.", "Василий Усов", 1179.99d));
        list.add(new Product("Swift 3. Разработка приложений в среде Xcode для iPhone и iPad с использованием iOS SDK", "Молли Маскри и др", 1617d));
        return list;
    }
}
