package com.example.partdtaskone;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class ListFragment extends Fragment implements OnItemClickListener{

    private RecyclerView recyclerView;
    private ProductAdapter productAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list, container, false);
        recyclerView = root.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));
        productAdapter = new ProductAdapter(root.getContext(), Generator.generate(), this);
        recyclerView.setAdapter(productAdapter);
        return root;
    }

    @Override
    public void onItemClick(Product bookTitle) {
        new AlertDialog.Builder(getContext())
                .setTitle("Информация")
                .setMessage("Название: " + bookTitle.getTitle() + "\nАвтор(ы): " + bookTitle.getAuthor() + " \nЦена: " + bookTitle.getPrice() + " грн.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }
}
