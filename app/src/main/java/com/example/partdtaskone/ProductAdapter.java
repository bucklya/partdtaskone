package com.example.partdtaskone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    private Context context;
    private List<Product> list;
    private OnItemClickListener listener;

    public ProductAdapter(Context context, List<Product> list, OnItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_products, parent, false);
        return new ProductViewHolder(v, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Product bookTitle = list.get(position);
        //holder.title.setText(bookTitle.getTitle());
        holder.bind(bookTitle);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        OnItemClickListener listener;
        View root;

        public ProductViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            this.listener = listener;
            title = itemView.findViewById(R.id.product_name);
            root = itemView.findViewById(R.id.root);
        }

        public void bind(final Product bookTitle) {
            title.setText(bookTitle.getTitle());
            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(bookTitle);
                }
            });
        }

    }
}


